package org.i9.slb.platform.anseko.provider;

import org.i9.slb.platform.anseko.provider.dto.CommandDispatchDto;

import java.util.HashMap;

/**
 * 命令处理远程调用服务类
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 17:48
 */
public interface IDubboCommandRemoteService {
    /**
     * 发起一个命令调试指令
     *
     * @param commandDispatchDto
     */
    void launchCommandDispatch(CommandDispatchDto commandDispatchDto);

    /**
     * 回调命令指令结果
     *
     * @param params
     */
    void callbackCommandExecute(HashMap<String, Object> params);
}
