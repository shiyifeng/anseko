package org.i9.slb.platform.anseko.provider;

import org.i9.slb.platform.anseko.provider.dto.SimulatorDto;

import java.util.List;

/**
 * 模拟器远程调用服务类
 *
 * @author R12
 * @date 2018年9月4日 10:38:37
 */
public interface IDubboSimulatorRemoteService {

    /**
     * 获取所有模拟器列表
     *
     * @return
     */
    List<SimulatorDto> getSimulatorDtoList();

    /**
     * 获取模拟器详情
     *
     * @param simulatorId
     * @return
     */
    SimulatorDto getSimulatorDtoInfo(String simulatorId);

    /**
     * 保存模拟器
     *
     * @param simulatorName
     * @param instance
     */
    SimulatorDto createSimulatorInfo(String simulatorName, String instance);

    /**
     * 删除模拟器
     *
     * @param simulatorId
     */
    void removeSimulatorInfo(String simulatorId);

    /**
     * 更新模拟器状态
     *
     * @param simulatorId
     * @param powerState
     */
    void refreshSimulatorPowerState(String simulatorId, int powerState);
}
