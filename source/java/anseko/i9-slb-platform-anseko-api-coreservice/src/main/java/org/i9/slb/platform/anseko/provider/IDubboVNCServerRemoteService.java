package org.i9.slb.platform.anseko.provider;

import org.i9.slb.platform.anseko.provider.dto.KeyValuesDto;

/**
 * vnc信息远程调用服务类
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/31 13:41
 */
public interface IDubboVNCServerRemoteService {

    /**
     * 获取模拟器VNC服务信息
     *
     * @param simulatorName
     * @return
     */
    KeyValuesDto simulatorVNCKeyValues(String simulatorName);
}
