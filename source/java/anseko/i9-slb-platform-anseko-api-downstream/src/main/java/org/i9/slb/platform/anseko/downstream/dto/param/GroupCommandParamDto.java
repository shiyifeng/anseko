package org.i9.slb.platform.anseko.downstream.dto.param;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * 文件命令执行传输对象
 *
 * @author R12
 * @date 2018.08.28
 */
public class GroupCommandParamDto implements java.io.Serializable {

    private static final long serialVersionUID = 4606057537000871228L;

    private String groupId;

    private List<CommandParamDto> commands;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @JSONField(serialize = false)
    public List<CommandParamDto> getCommands() {
        return commands;
    }

    public void setCommands(List<CommandParamDto> commands) {
        this.commands = commands;
    }
}
