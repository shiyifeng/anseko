package org.i9.slb.platform.anseko.common;

/**
 * 业务异常类
 *
 * @author R12
 * @date 2018.08.28
 */
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = -7687833436092301450L;

    private int result;

    private String message;

    public BusinessException(int result) {
        this.result = result;
        this.message = "error";
    }

    public BusinessException(int result, String message) {
        this.result = result;
        this.message = message;
    }

    public BusinessException(int result, String message, Throwable cause) {
        super(cause);
        this.result = result;
        this.message = message;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
