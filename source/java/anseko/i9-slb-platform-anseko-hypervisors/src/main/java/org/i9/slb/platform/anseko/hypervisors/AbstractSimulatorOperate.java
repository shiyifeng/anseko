package org.i9.slb.platform.anseko.hypervisors;

/**
 * 虚拟化操作类
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 10:48
 */
public abstract class AbstractSimulatorOperate {

    /**
     * 虚拟化操作类构造函数
     *
     * @param name
     */
    public AbstractSimulatorOperate(String name) {
        this.name = name;
    }

    public String name;

    /**
     * 启动模拟器
     */
    public String startSimulator() {
        return this.startSimulator(this.name);
    }

    /**
     * 启动模拟器
     *
     * @param name
     */
    public abstract String startSimulator(String name);

    /**
     * 重启模拟器
     */
    public String[] rebootSimulator() {
        return this.rebootSimulator(this.name);
    }

    /**
     * 重启模拟器
     *
     * @param name
     */
    public abstract String[] rebootSimulator(String name);

    /**
     * 关闭模拟器
     */
    public String shutdownSimulator() {
        return this.shutdownSimulator(this.name);
    }

    /**
     * 关闭模拟器
     *
     * @param name
     */
    public abstract String shutdownSimulator(String name);

    /**
     * 取消定义模拟器命令
     *
     * @return
     */
    public abstract String[] destroySimulator();

    /**
     * 创建物理磁盘
     *
     * @return
     */
    public abstract String[] createDiskPath();
}
