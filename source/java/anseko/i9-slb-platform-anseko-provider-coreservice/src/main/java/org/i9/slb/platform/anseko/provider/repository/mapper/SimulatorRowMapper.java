package org.i9.slb.platform.anseko.provider.repository.mapper;

import org.i9.slb.platform.anseko.provider.dto.SimulatorDto;
import org.i9.slb.platform.anseko.common.DateUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Simulator数据映射
 *
 * @author R12
 * @date 2018年9月6日 16:51:25
 */
public class SimulatorRowMapper implements RowMapper<SimulatorDto> {

    @Override
    public SimulatorDto mapRow(ResultSet resultSet, int i) throws SQLException {
        SimulatorDto simulatorDto = new SimulatorDto();
        simulatorDto.setId(resultSet.getString("id"));
        simulatorDto.setSimulatorName(resultSet.getString("simulatorName"));

        Date createDate = resultSet.getDate("createDate");
        simulatorDto.setCreateDate(DateUtil.format(createDate));
        simulatorDto.setCpunum(resultSet.getInt("cpunum"));
        simulatorDto.setRamnum(resultSet.getInt("ramnum"));
        simulatorDto.setPowerStatus(resultSet.getInt("powerStatus"));
        simulatorDto.setVncpassword(resultSet.getString("vncpassword"));
        simulatorDto.setVncport(resultSet.getInt("vncport"));
        simulatorDto.setAndroidVersion(resultSet.getString("androidVersion"));
        simulatorDto.setInstance(resultSet.getString("instance"));

        return simulatorDto;
    }
}
