package org.i9.slb.platform.anseko.provider.service;

import org.i9.slb.platform.anseko.provider.IDubboCommandRemoteService;
import org.i9.slb.platform.anseko.provider.dto.CommandDispatchDto;
import org.i9.slb.platform.anseko.provider.dto.CommandExecuteDto;
import org.i9.slb.platform.anseko.provider.repository.CommandDispatchRepository;
import org.i9.slb.platform.anseko.provider.repository.CommandExecuteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * 命令处理调度
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 17:48
 */
@Service("dubboCommandRemoteService")
public class DubboCommandRemoteService implements IDubboCommandRemoteService {

    @Autowired
    private CommandDispatchRepository commandDispatchRepository;

    @Autowired
    private CommandExecuteRepository commandExecuteRepository;

    /**
     * 发起一个命令调试指令
     *
     * @param commandDispatchDto
     */
    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void launchCommandDispatch(CommandDispatchDto commandDispatchDto) {
        this.commandDispatchRepository.insertCommandDispatch(commandDispatchDto);
        if (commandDispatchDto.getCommandExecuteDtos() == null || commandDispatchDto.getCommandExecuteDtos().isEmpty()) {
            return;
        }
        for (CommandExecuteDto commandExecuteDto : commandDispatchDto.getCommandExecuteDtos()) {
            commandExecuteDto.setCommandGroupId(commandDispatchDto.getCommandGroupId());
            this.commandExecuteRepository.insertCommandExecute(commandExecuteDto);
        }
    }

    /**
     * 回调命令指令结果
     *
     * @param params
     */
    @Transactional(rollbackFor = Throwable.class)
    @Override
    public synchronized void callbackCommandExecute(HashMap<String, Object> params) {
        String commandId = (String) params.get("commandId");
        String commandResult = (String) params.get("commandResult");

        // 如果当前命令已完成，则不进行下面处理
        CommandExecuteDto commandExecuteDto = this.commandExecuteRepository.queryCommandExecuteListByCommandId(commandId);
        if (commandExecuteDto.getStatus() == 1) {
            return;
        }
        // 更新当前命令状态
        this.commandExecuteRepository.updateCommandExecute(commandId, commandResult);

        // 获取执行组中所有命令判断是否全部完成
        List<CommandExecuteDto> commandExecuteDtos = this.commandExecuteRepository.queryCommandExecuteListByGroupId(commandExecuteDto.getCommandGroupId());
        int success = 1;
        for (CommandExecuteDto commandExecuteDto0 : commandExecuteDtos) {
            if (commandExecuteDto0.getStatus() == 0) {
                success = 0;
                break;
            }
        }

        if (success == 0) {
            return;
        }
        this.commandDispatchRepository.updateCommandDispatchEndTime(commandExecuteDto.getCommandGroupId(), success);
    }
}
