package org.i9.slb.platform.anseko.provider.remote;

import org.i9.slb.platform.anseko.downstream.dto.result.CommandExecuteReDto;
import org.i9.slb.platform.anseko.provider.IDubboCommandRemoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * 调用coreservice远程服务
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 18:16
 */
@Service
public class CoreserviceRemoteService {

    @Autowired
    private IDubboCommandRemoteService dubboCommandRemoteService;

    public void remoteServiceCallbackCommandDispatch(CommandExecuteReDto commandExecuteReDto) {
        HashMap<String, Object> param = new HashMap<String, Object>();
        param.put("commandId", commandExecuteReDto.getCommandId());
        param.put("commandResult", commandExecuteReDto.getExecuteResult());
        try {
            dubboCommandRemoteService.callbackCommandExecute(param);
        } catch (Exception e) {
            LOGGER.info("执行命令回调失败, param : {}", param);
        }
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(CoreserviceRemoteService.class);
}
