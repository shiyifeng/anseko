package org.i9.slb.platform.anseko.provider.task;

import org.i9.slb.platform.anseko.provider.utils.FileCommandUtil;
import org.i9.slb.platform.anseko.provider.utils.ShellCommandUtil;
import org.i9.slb.platform.anseko.common.BusinessException;
import org.i9.slb.platform.anseko.common.ErrorCode;
import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.downstream.dto.param.FileCommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.param.GroupCommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.param.ShellCommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.result.CommandExecuteReDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * group命令执行任务
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 17:12
 */
public class MultipleCommandExecuteTask implements Callable<DubboResult<List<CommandExecuteReDto>>> {

    private GroupCommandParamDto groupCommandParamDto;

    public MultipleCommandExecuteTask(GroupCommandParamDto groupCommandParamDto) {
        this.groupCommandParamDto = groupCommandParamDto;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(MultipleCommandExecuteTask.class);

    @Override
    public DubboResult<List<CommandExecuteReDto>> call() throws Exception {
        DubboResult<List<CommandExecuteReDto>> dubboResult = new DubboResult<List<CommandExecuteReDto>>();
        if (groupCommandParamDto.getCommands().isEmpty()) {
            return dubboResult;
        }
        List<CommandExecuteReDto> commandExecuteReDtos = new ArrayList<CommandExecuteReDto>();
        for (Object o : groupCommandParamDto.getCommands()) {
            CommandExecuteReDto commandExecuteReDto = new CommandExecuteReDto();
            try {
                if (o instanceof ShellCommandParamDto) {
                    ShellCommandParamDto shellCommandParamDto = (ShellCommandParamDto) o;
                    commandExecuteReDto = ShellCommandUtil.shellExecuteLocal(shellCommandParamDto);
                } else if (o instanceof FileCommandParamDto) {
                    FileCommandParamDto fileCommandParamDto = (FileCommandParamDto) o;
                    commandExecuteReDto = FileCommandUtil.fileExecuteLocal(fileCommandParamDto);
                } else {
                    throw new BusinessException(ErrorCode.UNKNOWN_COMMAND_ERROR, "未知命令");
                }
            } catch (BusinessException e) {
                dubboResult.setCode(e.getResult());
                dubboResult.setMsg(e.getMessage());
                LOGGER.info("multipleCommandExecute error ", e);
            } catch (Exception e) {
                dubboResult.setCode(ErrorCode.UNKOWN_ERROR);
                LOGGER.info("multipleCommandExecute error ", e);
            }
            if (dubboResult.getCode().intValue() != ErrorCode.SUCCESS.intValue()) {
                return dubboResult;
            }
            commandExecuteReDtos.add(commandExecuteReDto);
        }
        dubboResult.setRe(commandExecuteReDtos);
        return dubboResult;
    }
}
